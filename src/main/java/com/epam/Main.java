package com.epam;

import com.epam.view.ViewTask3;
import com.epam.view.ViewTask4;
import com.epam.view.ViewTask1;

public class Main {
    public static void main(String[] args) {
        ViewTask1.executeTask1();
        ViewTask3.showResults();
        ViewTask4.showResults();
    }
}
