package com.epam.controller;

import com.epam.model.MyInterface;

import java.util.Comparator;
import java.util.stream.Stream;

public class ControllerTask1 {
    public static MyInterface countMaxValue() {
        MyInterface varMax = (a, b, c) -> Stream.of(a, b, c).max(Comparator.naturalOrder()).get();
        return varMax;
    }

    public static MyInterface averageValue() {
        MyInterface varAverage = (a, b, c) -> (a + b + c) / 3;
        return varAverage;
    }
}
