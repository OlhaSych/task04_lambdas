package com.epam.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ControllerTask4 {
    private static Scanner scanner = new Scanner(System.in);
    private static List<String> text = new ArrayList<>();

    public static long countNumberOfUniqueWords(List<String> list) {
        return list.stream().distinct().count();
    }

    public static List<String> sortUniqueWords(List<String> list) {
        list.stream().filter(word -> !list.contains(word)).forEach(list::add);
        return list.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
    }

    public static List<String> readText() {
        while (true) {
            String word = scanner.nextLine();
            if (word.equals("")) {
                break;
            } else {
                text.add(word);
            }
        }
        return text.stream().flatMap(e -> Stream
                .of(e.split(" "))).collect(Collectors.toList());
    }


}
