package com.epam.controller;

import com.epam.model.Command;
import com.epam.model.CommandImpl;

import java.util.HashMap;
import java.util.Map;

public class ControllerTask2 {
    private final Map<String, Command> map = new HashMap<>();
    private Command command = new CommandImpl();

    public void runTask2() {
        map.put("one", text -> command.execute(text));
        map.put("two", command);
        map.put("three", new Command() {
            @Override
            public void execute(String s) {
                System.out.println(s);
            }
        });
        map.put("four", System.out::print);
    }
}
