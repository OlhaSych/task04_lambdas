package com.epam.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ControllerTask3 {

    public static List<Integer> randomIntFirst() {
        return Stream.generate(new Random()::nextInt)
                .limit(10).collect(Collectors.toList());
    }

    public static List<Integer> randomIntSecond() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int randomNumber = (int) (Math.random() * 5000);
            list.add(i, randomNumber);
        }
        return list.stream().sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }
}
