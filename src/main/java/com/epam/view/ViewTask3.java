package com.epam.view;

import com.epam.controller.ControllerTask3;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.IntSummaryStatistics;
import java.util.List;

public class ViewTask3 {
    private static Logger logger = LogManager.getLogger(ViewTask1.class);

    public static void showResults() {
        List<Integer> list1 = ControllerTask3.randomIntFirst();
        List<Integer> list2 = ControllerTask3.randomIntSecond();
        IntSummaryStatistics statistics = list2.stream()
                .mapToInt(a -> a).summaryStatistics();
        logger.info(list2 + "\n" + statistics);
        Integer min = list1.stream()
                .reduce(Integer.MAX_VALUE, Integer::min);
        logger.info(list1 + "\n" + "min = " + min);
    }
}
