package com.epam.view;

import com.epam.controller.ControllerTask1;
import com.epam.model.MyInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ViewTask1 {
    private static Logger logger = LogManager.getLogger(ViewTask1.class);

    public static void executeTask1() {
        logger.info("Max variables of 23, 67, 20 : " +
                ControllerTask1.countMaxValue().execute(23, 67, 20));
        logger.info("Average  of 3, 6, 9 : " +
                ControllerTask1.averageValue().execute(3, 6, 9));
    }
}
