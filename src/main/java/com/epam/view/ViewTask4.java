package com.epam.view;

import com.epam.controller.ControllerTask4;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ViewTask4 {
    private static Logger logger = LogManager.getLogger(ViewTask1.class);

    public static void showResults() {
        logger.info("Please enter your text: ");
        List<String> text = ControllerTask4.readText();
        long countNumOfUniqueWords = ControllerTask4.countNumberOfUniqueWords(text);
        logger.info("number of unique words =" + countNumOfUniqueWords);
        List<String> sortedList = ControllerTask4.sortUniqueWords(text);
        logger.info("sorted words :" + "\n" + sortedList);
    }
}
