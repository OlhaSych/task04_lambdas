package com.epam.model;

public interface Command {
    void execute(String s);
}
