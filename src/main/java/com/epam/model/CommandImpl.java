package com.epam.model;

public class CommandImpl implements Command {
    @Override
    public void execute(String s) {
        System.out.println(s);
    }
}
