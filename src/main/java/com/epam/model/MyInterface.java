package com.epam.model;

@FunctionalInterface
public interface MyInterface {
    int execute(int a, int b, int c);
}
